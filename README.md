# The purpose of this tutorial

In this tutorial, I present a simplish way of how to match medication data inputted by participants or nurses with ATC codes retrieved from a separate list.

As a bonus, we practice how to use the “here” package that helps managing file paths in multi-platform projects where co-authors might use e.g. MacOS, Windows or Linux.

# The only things you need for this tutorial are:

- a computer with R installed on it
- RStudio or similar IDE
- an internet access for downloading a list from a national pharmaceuticals pricing board containing medication names and corresponding ATC codes.

# Download an Excel file containing a list of medications and their corresponding ATC codes from Hila

For matching drug names with corresponding ATC codes, I personally use a combination of more than 10 Excel files from different years, all downloaded from the national Pharmaceuticals Pricing Board’s (abbr. “Hila’s”) website.

However, To make things simpler in this example, only one Excel file from Hila’s website will be downloaded and imported to R. So go ahead and download e.g. the file “February-1st-2023-February-3rd-2023.xlsx” from the following website:

https://www.hila.fi/en/notices/reimbursable-authorized-medicinal-products-and-their-prices/

# Create a new project for this practice in RStudio

You may either:

- clone this whole practice compendium from GitLab
- create manually a new R project in RStudio and move the needed files manually to new subfolders under the root directory of that R project.

As for the recommended method 1 above, cloning with git is very easy. I have earlier made a fast tutorial of git, covering also cloning:

https://vldesign.kapsi.fi/git-guide/

The manual method is explained below.

# Creating the subfolders manually and copying the files to right subfolders

If you want to do this practice the hard way, you can create the folder tree by hand. There are only a few files, so even this method is easy and fast:

Create a new R project in RStudio by selecting File -> New Project -> New Directory -> New Project.

Give your project some name, e.g. “atc_matching_practice”. A new folder called “atc_matching_practice” will be created. Also, a new file called “atc_matching_practice.Rproj” will be created in that folder.

Place the files of this practice to the right folders according to the image below.


<p align="center">
  <img src="tree_image.png" width="350" title="Tree">
</p>


